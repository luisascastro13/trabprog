package trabprog;

/**
 * @author Luísa
 * Classe que possui todas as características de um Atirador
 */
class Atirador extends Unidade {
    Atirador (){
    }
    @Override //herda ataqueEspecial de Unidade, e sobrescreve dentro de Atirador
    public void ataqueEspecial(Unidade unidade){
        unidade.setVida(0);
    }
    @Override //Setta o ataque e a vida de um Atirador criado
    public void setTudo(){
        this.setAtaque(Dados.getaAtirador());
        this.setVida(Dados.getvAtirador());
    }
    @Override
    public void atacar(Unidade unidade){
        if(!this.isMorto()){
            unidade.setVida(unidade.getVida() - this.getAtaque());
        } 
    }
}
