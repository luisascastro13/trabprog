/**
 * @author Luísa
 * Classe controladora do FXML Atirador
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class AtiradorController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label quantAtirador;
    
    @FXML
    private TextField vida, ataque;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        quantAtirador.setText(Integer.toString(Dados.getQuantAtirador()));
    }    
    
    /**
    * Botão voltar no FXML, que setta os dados e troca de tela
    */
    @FXML
    public void voltar()
    {
        Dados.setvAtirador(Integer.parseInt(vida.getText()));
        Dados.setaAtirador(Integer.parseInt(ataque.getText()));
        TrabProg.trocaTela("TelaInicial.fxml");
    }
    
}
