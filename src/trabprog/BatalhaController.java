/**
 * @author Luísa
 * Classe controladora do FXML Batalha
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class BatalhaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    public void verResultados(){
        if(Dados.getQuemGanhou().equals("ganhou"))
        {
            TrabProg.trocaTela("TelaVitoria.fxml");
        }
        else if(Dados.getQuemGanhou().equals("perdeu")){
            TrabProg.trocaTela("TelaDerrota.fxml");
        }
    }
    
}