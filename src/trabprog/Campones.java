package trabprog;

/**
 * @author Luísa
 * Classe que contém todas especificações de um Camponês
 */
class Campones extends Unidade{  
    int contadorDeAtaqueEspecial=0;
    Campones()
    {       
    }
    @Override
    public void atacar(Unidade unidade){
        if(!this.isMorto()){
            if(contadorDeAtaqueEspecial==2 || contadorDeAtaqueEspecial==1)
            {
               
               contadorDeAtaqueEspecial=2;
            }
            else{
                unidade.setVida(unidade.getVida() - this.getAtaque());
            }           
        } 
    }
    @Override //Herda de Unidade e sobrescreve com devidos valores
    public void ataqueEspecial(Unidade unidade){
        unidade.setVida(unidade.getVida() - this.getAtaque()*2);
    }
    @Override //setta valores de ataque e vida para um Camponês
    public void setTudo(){
        this.setAtaque(Dados.getaCampones());
        this.setVida(Dados.getvCampones());
    }
}
