/**
 * @author Luísa
 * Classe controladora do FXML Camponês
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CamponesController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label quantCampones;
    
    @FXML
    private TextField vida, ataque;
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        quantCampones.setText(Integer.toString(Dados.getQuantCampones()));
    }  
    
    /**
    * Botão no FXML que salva todos os dados escritos pelo usuário
    * Setta os mesmos em Dados e troca de tela
    */
    @FXML
    public void voltar()
    {
        Dados.setvCampones(Integer.parseInt(vida.getText()));
        Dados.setaCampones(Integer.parseInt(ataque.getText()));
        
        TrabProg.trocaTela("TelaInicial.fxml");
    }   
    
}
