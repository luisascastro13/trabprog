/**
 * @author Luísa
 * Classe que contém todas especificidades de um Cavaleiro
 */
package trabprog;

class Cavaleiro extends Unidade{
    int contadorDeAtaqueEspecial=0;
    Cavaleiro()
    {        
    }    
    @Override //herda de Unidade e sobrescreve o método ataqueEspecial()
    public void ataqueEspecial(Unidade unidade){
        unidade.setVida((int)(Math.round(this.getAtaque()*0.2)));
    }
    @Override
    public void atacar(Unidade unidade){
        if(!this.isMorto()){
            if(contadorDeAtaqueEspecial==2 || contadorDeAtaqueEspecial==1)
            {
               unidade.setVida((int)(Math.round(this.getAtaque()*0.2)));
               contadorDeAtaqueEspecial--;
            }
            else{
                unidade.setVida(this.getAtaque());
            }           
        } 
    }
    @Override //setta valores conforme os que o usuário decidiu
    public void setTudo(){
        this.setAtaque(Dados.getaCavaleiro());
        this.setVida(Dados.getvCavaleiro());
    }
      
}
