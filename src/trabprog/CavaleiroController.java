/**
 * @author Luísa
 * Classe controladora do FXML Cavaleiro
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CavaleiroController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Label quantCavaleiro;
    
    @FXML
    private TextField vida, ataque;
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        quantCavaleiro.setText(Integer.toString(Dados.getQuantCavaleiro()));
    }  
    
    /**
    * Botão no FXML que salva os valores escritos pelo usuário e troca de tela
    */
    @FXML
    public void voltar()
    {
        Dados.setvCavaleiro(Integer.parseInt(vida.getText()));
        Dados.setaCavaleiro(Integer.parseInt(ataque.getText()));
        
        TrabProg.trocaTela("TelaInicial.fxml");
    }    
}
