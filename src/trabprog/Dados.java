/**
 * @author Luísa
 * A classe é utilizada para salvar quantidades e
 * valores importantes para um bom funcionamento
 * do jogo, tais como: quantidade de Personagens que
 * o usuário deseja, quanto vale a vida de cada um, mesmo
 * que o usuário não modifique, e obviamente, o ganhador do jogo
 */
package trabprog;

public class Dados { //Todas as funções são basicamente getters e setters dos valores, ou então de sua soma
    private static int quantMedico, quantCavaleiro, quantAtirador, quantCampones;
    private static int vMedico =80, vCavaleiro = 100, vAtirador = 60, vCampones = 70;
    private static int aMedico = 35, aCavaleiro = 40, aAtirador = 60, aCampones =50;
    private static int dificuldade;
    private static String quemGanhou;

    /**
     * 
     * @return dificuldade
     */
    public static int getDificuldade() {
        return dificuldade;
    }

    /**
     * 
     * @param dificuldade 
     */
    public static void setDificuldade(int dificuldade) {
        Dados.dificuldade = dificuldade;
    }   

    /**
     * 
     * @return quemGanhou
     */
    public static String getQuemGanhou() {
        return quemGanhou;
    }

    /**
     * 
     * @param quemGanhou 
     */
    public static void setQuemGanhou(String quemGanhou) {
        Dados.quemGanhou = quemGanhou;
    }
    
    
    /**
     * 
     * @return int vMedico, que significa a vida base de um médico
     */
    public static int getvMedico() {
        return vMedico;
    }
    /**
     * 
     * @return int vCavaleiro, que significa a vida base de um cavaleiro
     */
    public static int getvCavaleiro() {
        return vCavaleiro;
    }
    /**
     * 
     * @return int vAtirador, que significa a vida base de um atirador 
     */
    public static int getvAtirador() {
        return vAtirador;
    }

    /**
     * 
     * @return int vCampones, que significa a vida base de um campones
     */
    public static int getvCampones() {
        return vCampones;
    }

    /**
     * 
     * @return int aMedico, que significa o ataque base de um medico
     */
    public static int getaMedico() {
        return aMedico;
    }

    /**
     * 
     * @return int aCavaleiro, que significa o ataque base de um cavaleiro
     */
    public static int getaCavaleiro() {
        return aCavaleiro;
    }

    /**
     * 
     * @return int aAtirador, que significa o ataque base de um atirador
     */
    public static int getaAtirador() {
        return aAtirador;
    }

    /**
     * 
     * @return int aCampones, que significa o ataque base de um campones
     */
    public static int getaCampones() {
        return aCampones;
    }

    /**
     * 
     * @param vMedico vida base de um medico 
     */
    public static void setvMedico(int vMedico) {;
        Dados.vMedico = vMedico;
    }

    /**
     * 
     * @param vCavaleiro vida base de um cavaleiro
     */
    public static void setvCavaleiro(int vCavaleiro) {
        Dados.vCavaleiro = vCavaleiro;
    }

    /**
     * 
     * @param vAtirador vida base de um atirador
     */
    public static void setvAtirador(int vAtirador) {
        Dados.vAtirador = vAtirador;
    }

    /**
     * 
     * @param vCampones vida base de um campones 
     */
    public static void setvCampones(int vCampones) {
        Dados.vCampones = vCampones;
    }

    /**
     * 
     * @param aMedico ataque base de um medico
     */
    public static void setaMedico(int aMedico) {
        Dados.aMedico = aMedico;
    }

    /**
     * 
     * @param aCavaleiro ataque base de um cavaleiro
     */
    public static void setaCavaleiro(int aCavaleiro) {
        Dados.aCavaleiro = aCavaleiro;
    }

    /**
     * 
     * @param aAtirador ataque base de um atirador
     */
    public static void setaAtirador(int aAtirador) {
        Dados.aAtirador = aAtirador;
    }

    /**
     * 
     * @param aCampones ataque base de um campones
     */
    public static void setaCampones(int aCampones) {
        Dados.aCampones = aCampones;
    }
    
    /**
     * 
     * @return quantMedico quantidade de medico
     */
    public static int getQuantMedico() {
        return quantMedico;
    }

    /**
     * 
     * @param quantMedico quantidade de medicos
     */
    public static void setQuantMedico(int quantMedico) {
        Dados.quantMedico = quantMedico;
    }

    /**
     * 
     * @return quantCavaleiro quantidade de cavaleiros
     */
    public static int getQuantCavaleiro() {
        return quantCavaleiro;
    }

    /**
     * 
     * @param quantCavaleiro quantidade de cavaleiros
     */
    public static void setQuantCavaleiro(int quantCavaleiro) {
        Dados.quantCavaleiro = quantCavaleiro;
    }

    /**
     * 
     * @return quantAtirador quantidade de atiradores
     */
    public static int getQuantAtirador() {
        return quantAtirador;
    }

    /**
     * 
     * @param quantAtirador quantidade de atiradores
     */
    public static void setQuantAtirador(int quantAtirador) {
        Dados.quantAtirador = quantAtirador;
    }

    /**
     * 
     * @return quantCampones quantidade de camponeses
     */
    public static int getQuantCampones() {
        return quantCampones;
    }

    /**
     * 
     * @param quantCampones quantidade de camponeses
     */
    public static void setQuantCampones(int quantCampones) {
        Dados.quantCampones = quantCampones;
    }
    /**
     * 
     * @return soma das quantidades de personagens que serao criados
     */
    public static int getSoma()
    {
        return quantMedico + quantAtirador + quantCampones + quantCavaleiro;
    }
}
