package trabprog;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import java.util.*;
/**
 * @author Luísa
 * Classe controladora do FXML Guerra
 */
public class GuerraController implements Initializable {
    
    private ArrayList<Unidade> esquadrao = new ArrayList();
    private ArrayList<Unidade> esquadraoInimigo = new ArrayList();
    @FXML
    private Label quantMedico, quantCampones, quantCavaleiro, quantAtirador;
    @FXML
    private Label quantMedicoOponente, quantCamponesOponente, quantCavaleiroOponente, quantAtiradorOponente;
    private int pontoEsquadraoInimigo=0, pontoEsquadrao=0;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Pega os valores dos Dados e coloca na tela para o usuário ver
        quantMedico.setText(Integer.toString(Dados.getQuantMedico()));
        quantCampones.setText(Integer.toString(Dados.getQuantCampones()));
        quantCavaleiro.setText(Integer.toString(Dados.getQuantCavaleiro()));
        quantAtirador.setText(Integer.toString(Dados.getQuantAtirador()));
        
        criarEsquadroes(); //cria esquadroes
        batalhar(); //faz os esquadroes batalharem
      
    }
    /**
    * @author Luísa   
    * @param dados representa a quantidade escolhida pelo usuario a ser criado o seu proprio batalhao 
    * @return valor que representa a quantidade de unidades a serem criadas
    * A função serve para formar o exército do oponente (no caso, o computador).
    * Usa como base os valores criados para o esquadrão do jogador e retira um x valor randomico
    */
    public int formacaoExercitoOponenteFacil(int dados)
    {
        Random random = new Random();
        int intervaloRandomico = random.nextInt(10);
        int valor;
        if(dados >= 10){            
            valor = dados - intervaloRandomico;
        }
        else{
            intervaloRandomico = random.nextInt(dados);
            valor = dados + intervaloRandomico;
        }
        return valor;
    }
    /**
    * @author Luísa   
    * @param dados representa a quantidade escolhida pelo usuario a ser criado o seu proprio batalhao 
    * @return valor que representa a quantidade de unidades a serem criadas
    * A função serve para formar o exército do oponente (no caso, o computador).
    * Usa como base os valores criados para o esquadrão do jogador e retira um x valor randomico
    */
    public int formacaoExercitoOponenteMedio(int dados)
    {
        Random random = new Random();
        int intervaloRandomico = random.nextInt(2);
        int somaOuSubtrai = random.nextInt(1);
        int valor;
        if(dados > 10){
            if(somaOuSubtrai == 0)
            {
                valor = dados - intervaloRandomico;   
            }
            else
            {
               valor = dados + intervaloRandomico;
            }
        }
        else{
            valor = dados + intervaloRandomico;
        }
        return valor;   
    }
    
    /**
    * @author Luísa  
    * @param dados representa a quantidade escolhida pelo usuario a ser criado o seu proprio batalhao 
    * @return int valor que representa a quantidade de unidades a serem criadas
    * A função serve para formar o exército do oponente (no caso, o computador).
    * Usa como base os valores criados para o esquadrão do jogador e retira um x valor randomico
    */
    public int formacaoExercitoOponenteDificil(int dados){
        Random random = new Random();
        int intervaloRandomico = random.nextInt(4);
        int valor;
        valor = dados + intervaloRandomico;
        return valor;
    }
    
    
    /**
     * @param indice inteiro, que pega o indice do esquadrao para batalhar um com o outro
     * @return boolean que diz qual dos guerreiros morreu
     * se o retorno for "true", quer dizer que o meu guerreiro morreu
     * se for falso, o guerreiro oponente morreu
     * Os ataques entre cada personagem acontecem até que algum dos dois morra.
     */
    public boolean batalhaIndividual(int indice){
        Random random = new Random();
        int intervalo = random.nextInt(1);
        System.out.println(intervalo);
        if(intervalo == 0){
            esquadraoInimigo.get(indice).atacar(esquadrao.get(indice));
        }        
        while(!esquadraoInimigo.get(indice).isMorto() && !esquadrao.get(indice).isMorto())
        {
            esquadrao.get(indice).atacar(esquadraoInimigo.get(indice));
            esquadraoInimigo.get(indice).atacar(esquadrao.get(indice));
        }
        return esquadrao.get(indice).isMorto();
    }
    
    /**
     * @param indice inteiro, que pega o indice dos esquadroes
     * se o personagem tiver morto, ele remove do ArrayListyList
     */
    public void limparMortos(int indice){
        if(esquadrao.get(indice).isMorto()){
            esquadrao.remove(indice);
        }
        if(esquadraoInimigo.get(indice).isMorto()){
            esquadraoInimigo.remove(indice);
        }
    }
    /**
    * Função para fazer com que cada "batalhaIndividual" seja realizada
    * até que qualquer um dos ArrayList esteja vazio
    */
    public void batalhar(){
        boolean quemMorreu;
        while(!esquadrao.isEmpty() && !esquadraoInimigo.isEmpty())
        { System.out.println("e");
            int menor = Math.min(esquadrao.size(), esquadraoInimigo.size());        
            for(int i=0; i<menor; i++)
            {
                quemMorreu = batalhaIndividual(i);
                if(quemMorreu){
                    pontoEsquadraoInimigo++; //esquadraoInimigo matou um personagem do meu esquadrao
                }
                else{
                    pontoEsquadrao++; //esquadrao matou um personagem do esquadraoInimigo
                }
                if(pontoEsquadrao>15*esquadraoInimigo.size()/100){ //se esquadrao matou mais de 15% do esquadraoInimigo
                    //atacar especial
                    esquadrao.get(i).ataqueEspecial(esquadraoInimigo.get(i));
                    pontoEsquadrao = 0;
                }
                if(pontoEsquadraoInimigo>15*esquadrao.size()/100){//se esquadraoInimigo matou mais de 15% do meu esquadrao
                    //atacar especial
                    esquadraoInimigo.get(i).ataqueEspecial(esquadrao.get(i));
                    pontoEsquadraoInimigo = 0;
                }
            }
            
            for(int i=menor-1; i>=0; i--)
            {
                limparMortos(i);
            }
        }
    }
    
    /**
    * Cria o esquadrão conforme o número definido pelo usuário
    * E o esquadraoInimigo conforme a função "criarExercitoOponente" e a dificuldade selecionada no inicio do jogo
    */
    public void criarEsquadroes(){
        //Criando esquadrão do jogador
        for(int i=0; i<Dados.getQuantMedico(); i++)
        {
            esquadrao.add(new Medico());
            esquadrao.get(i).setTudo();
        }
        for(int i=0; i<Dados.getQuantCampones(); i++)
        {
            esquadrao.add(new Campones());            
            esquadrao.get(i+Dados.getQuantMedico()).setTudo();
        }
        for(int i=0; i<Dados.getQuantCavaleiro(); i++)
        {
            esquadrao.add(new Cavaleiro());
            esquadrao.get(i+Dados.getQuantMedico() + Dados.getQuantCampones()).setTudo();
        }
        for(int i=0; i<Dados.getQuantAtirador(); i++)
        {
            esquadrao.add(new Atirador());
            esquadrao.get(i+Dados.getQuantMedico() + Dados.getQuantCampones() + Dados.getQuantCavaleiro()).setTudo();
        }      
        Collections.shuffle(esquadrao);  //embaralhando esquadrão
        
        //Criando o esquadrão do inimigo conforme a dificuldade        
        switch(Dados.getDificuldade())
        {
            case 1:
                int x, y=0;
                x = formacaoExercitoOponenteFacil(Dados.getQuantMedico());
                quantMedicoOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Medico());
                    esquadraoInimigo.get(i).setTudo();
                }
                y+=x;

                x = formacaoExercitoOponenteFacil(Dados.getQuantCampones());        
                quantCamponesOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Campones());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }
                y+=x;


                x = formacaoExercitoOponenteFacil(Dados.getQuantCavaleiro());        
                quantCavaleiroOponente.setText("" + x);        
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Cavaleiro());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }
                y+=x;

                x = formacaoExercitoOponenteFacil(Dados.getQuantAtirador());
                quantAtiradorOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Atirador());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }          
                y+=x; //soma de unidades do esquadraoInimigo

                Collections.shuffle(esquadraoInimigo);  //embaralhando o esquadrao do inimigo       
                break;
            case 2:
                y=0;
                x = formacaoExercitoOponenteMedio(Dados.getQuantMedico());
                quantMedicoOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Medico());
                    esquadraoInimigo.get(i).setTudo();
                }
                y+=x;

                x = formacaoExercitoOponenteMedio(Dados.getQuantCampones());        
                quantCamponesOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Campones());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }
                y+=x;


                x = formacaoExercitoOponenteMedio(Dados.getQuantCavaleiro());        
                quantCavaleiroOponente.setText("" + x);        
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Cavaleiro());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }
                y+=x;

                x = formacaoExercitoOponenteMedio(Dados.getQuantAtirador());
                quantAtiradorOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Atirador());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }          
                y+=x; //soma de unidades do esquadraoInimigo

                Collections.shuffle(esquadraoInimigo);  //embaralhando o esquadrao do inimigo       
                break;
            case 3:
                y=0;
                x = formacaoExercitoOponenteDificil(Dados.getQuantMedico());
                quantMedicoOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Medico());
                    esquadraoInimigo.get(i).setTudo();
                }
                y+=x;

                x = formacaoExercitoOponenteDificil(Dados.getQuantCampones());        
                quantCamponesOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Campones());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }
                y+=x;


                x = formacaoExercitoOponenteDificil(Dados.getQuantCavaleiro());        
                quantCavaleiroOponente.setText("" + x);        
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Cavaleiro());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }
                y+=x;

                x = formacaoExercitoOponenteDificil(Dados.getQuantAtirador());
                quantAtiradorOponente.setText("" + x);
                for(int i=0; i<x; i++)
                {
                    esquadraoInimigo.add(new Atirador());
                    esquadraoInimigo.get(i+(y)).setTudo();
                }          
                y+=x; //soma de unidades do esquadraoInimigo

                Collections.shuffle(esquadraoInimigo);  //embaralhando o esquadrao do inimigo       
        }
    }
    /**
    * Para mostrar os resultados do jogo, um botão no FXML, que muda a tela
    */
    public void mostraResultados(){
        //Avisa se ganhou ou perdeu em um sout
        if(esquadrao.isEmpty())
        {
            Dados.setQuemGanhou("perdeu");
        }
        if(esquadraoInimigo.isEmpty()){
            Dados.setQuemGanhou("ganhou");
        }
        TrabProg.trocaTela("Batalha.fxml");
    }
}
