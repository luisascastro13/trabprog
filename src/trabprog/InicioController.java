/**
 * @author Luísa
 * Classe controladora do FXML Inicio
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class InicioController implements Initializable {

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    
    
    /**
     * Método que controla o botão "jogar" na tela FXML
     */
    public void jogar(){
        TrabProg.trocaTela("Opcoes.fxml");
    }    
}
