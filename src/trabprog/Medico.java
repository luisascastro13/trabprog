/**
 * @author Luísa
 * Classe que contém as especificidades de um Medico
 */
package trabprog;

class Medico extends Unidade{    
    Medico(){
    }
    @Override //herda de Unidade e sobrescreve conforme a necessidade
    public void setTudo(){
        this.setAtaque(Dados.getaMedico());
        this.setVida(Dados.getvMedico());
    }
    @Override //herda de Unidade e sobrescreve para ser específico de Medico
    public void ataqueEspecial(Unidade unidade){
        if(!this.isMorto()){
            
            System.out.println("oi");
            this.setVida(Dados.getvMedico()/2 + Dados.getvMedico());
        } 
    }
    @Override
    public void atacar(Unidade unidade){
        if(!this.isMorto()){
            unidade.setVida(unidade.getVida() - this.getAtaque());
        } 
    }
}
