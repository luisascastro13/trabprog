/**
 * @author Luísa
 * Classe controladora do FXML Medico
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MedicoController implements Initializable {
   
    @FXML
    private Label quantMedico;
    
    @FXML
    private TextField vida, ataque;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        quantMedico.setText(Integer.toString(Dados.getQuantMedico()));
    }  
    /**
     * Botão que salva os valores dos TextFields e troca a tela
     */
    @FXML
    public void voltar()
    {
        Dados.setvMedico(Integer.parseInt(vida.getText()));
        Dados.setaMedico(Integer.parseInt(ataque.getText()));
        
        TrabProg.trocaTela("TelaInicial.fxml");
    }
}
