/**
 * @author Luísa
 * Classe controladora do FXML Opcoes
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class OpcoesController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    public void facil(){
        Dados.setDificuldade(1);
        TrabProg.trocaTela("TelaInicial.fxml");
    }
    public void medio(){
        Dados.setDificuldade(2);
        TrabProg.trocaTela("TelaInicial.fxml");
    }
    public void dificil(){
        Dados.setDificuldade(3);
        TrabProg.trocaTela("TelaInicial.fxml");
    }
    
}
