/**
 * @author Luísa e Matheus Almeida
 * Classe controladora do FXML Tela Inicial
 */
package trabprog;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class TelaInicialController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    } 
    @FXML
    private TextField quantMedico, quantCampones, quantCavaleiro, quantAtirador;
    
    /*
     * Botão no FXML que guarda na classe Dados o que
     * foi informado pelo usuário e troca de tela
     */
    @FXML
    public void irParaGuerraAcao(){
        Dados.setQuantMedico(Integer.parseInt(quantMedico.getText()));        
        Dados.setQuantCavaleiro(Integer.parseInt(quantCavaleiro.getText()));
        Dados.setQuantCampones(Integer.parseInt(quantCampones.getText()));
        Dados.setQuantAtirador(Integer.parseInt(quantAtirador.getText()));        
        TrabProg.trocaTela("Guerra.fxml");
    }
    /*
     * Botão cuja ação abre o FXML Medico, onde se pode alterar
     * valores de vida e ataque do personagem
     */
    @FXML
    public void verMedico(){
        Dados.setQuantMedico(Integer.parseInt(quantMedico.getText()));
        TrabProg.trocaTela("Medico.fxml");     
    }  
    /*
     * Botão cuja ação abre o FXML Campones, onde se pode alterar
     * valores de vida e ataque do personagem
     */
    @FXML
    public void verCampones(){
        Dados.setQuantCampones(Integer.parseInt(quantCampones.getText()));
        TrabProg.trocaTela("Campones.fxml");     
    }  
    /*
     * Botão cuja ação abre o FXML Atirador, onde se pode alterar
     * valores de vida e ataque do personagem
     */
    @FXML
    public void verAtirador(){
        Dados.setQuantAtirador(Integer.parseInt(quantAtirador.getText()));
        TrabProg.trocaTela("Atirador.fxml");     
    }  
    /*
     * Botão cuja ação abre o FXML Cavaleiro, onde se pode alterar
     * valores de vida e ataque do personagem
     */
    @FXML
    public void verCavaleiro(){
        Dados.setQuantCavaleiro(Integer.parseInt(quantCavaleiro.getText()));
        TrabProg.trocaTela("Cavaleiro.fxml");     
    }  
}
