/**
 * @author Luísa
 * Classe que contém o main, a função trocaTela e da launch nos args
 */
package trabprog;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TrabProg extends Application {    
    
    private static Stage stage;

    public static Stage getStage() { //getter do Stage
        return stage;
    }    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(TrabProg.class.getResource(tela));
            Scene scene = new Scene(root);
       
            stage.setScene(scene);
            stage.show();
        }catch(IOException e){
            System.out.println("Verificar arquivo FXML " + e);
        }
        
    }
    /**
     * @param stage
     * @throws java.lang.Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
        
        Scene scene = new Scene(root);
        TrabProg.stage = stage;
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       launch(args);      
    } 
}
