/**
 * @author Luísa
 * Classe abstrata que deixa como herança características para
 * todos os personagens do jogo. É nela que são descritas as funções
 * atacar e isMorto(), além de getters e setters básicos dos atributos
 */
package trabprog;

public abstract class Unidade {
    private int ataque, vida;
    public abstract void ataqueEspecial(Unidade unidade);
    public abstract void atacar(Unidade unidade);

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }
    
    @Override
    public String toString(){
        return getAtaque() + " " + getVida();
    }
    public abstract void setTudo();
    public boolean isMorto()
    {
        return getVida() <= 0;        
    }

}
